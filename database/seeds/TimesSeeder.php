<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TimesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 23; $i++) {
            $hour = ($i < 10) ? 0 . $i : $i;
            DB::table('times')->insert([
                ['time' => $hour . ':00',],
                ['time' => $hour . ':15',],
                ['time' => $hour . ':30',],
                ['time' => $hour . ':45',],
            ]);
        }
        DB::table('times')->insert([
            ['time' => '23:00',],
        ]);
    }
}
