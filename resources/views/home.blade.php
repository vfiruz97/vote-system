@extends('layouts.app')

@section('content')    
    <div class="container" ref="basedata" timelist='{!!  $timeList !!}' vote='[{!!  $vote !!}]' user='{!!  $user !!}'>
        <div class="row justify-content-center">

            <div v-if="!hasActiveVote" class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Cтарт голосования') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Продолжительность первой фазы</label>
                                    <select v-model="createVote.duePhase1" class="form-control">
                                        <option v-for="num in minutes" :key="num" :value="num">@{{ num }} мин.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Продолжительность второй фазы</label>
                                    <select v-model="createVote.duePhase2" class="form-control">
                                        <option v-for="num in minutes" :key="num" :value="num">@{{ num }} мин.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <br><button @click="createNewVote" class="btn btn-primary">Начать</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div v-if="hasActiveVote && isFirstPhaseVote" class="col-md-8 mt-4">
                <div class="card">
                    <div class="card-header">
                        <div style="text-align:center; display:flex; justify-content:space-between;">
                            <span>{{ __('Первая фаза голосования') }}<br>
                                <small>Создаль: @{{ activeVote['creatorName'] }}</small>
                            </span>
                            <span>Время до окончения первой фазы<br>@{{ activeVote['timer'] }} сек.</span>
                            <button v-if="activeVote['creatorId'] === user['id']" class="btn btn-primary btn-sm">Обновить
                                голосования</button><br>
                        </div>
                    </div>
                    <div class="card-body">
                        <div v-if="canVoteInFirstPhase" class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Время</label>
                                    <select v-model="createVoteFirstPhase.time" class="form-control">
                                        <option v-for="t in timeList" :value="t['id']">@{{ t['time'] }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="event">Мероприятие</label>
                                    <suggestions v-model="createVoteFirstPhase.event" v-bind:options="options"
                                        :on-input-change="onEventInputChange">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""></label><br>
                                <div class="col-md-12">
                                    <button @click="createVoteFirstPhaseForm()" class="btn btn-primary">Голосовать</button>
                                </div>
                            </div>
                        </div>
                        <div v-else>Ждите вторую фазу голосования!</div>
                    </div>
                </div>
            </div>

            <div v-if="hasActiveVote && (isSecondPhaseVote || isEndedVote)" class="col-md-8 mt-4">
                <div class="card">
                    <div class="card-header">
                        <div style="text-align:center; display:flex; justify-content:space-between;">
                            <span>
                                <span v-if="!isEndedVote">{{ __('Второя фаза голосования') }}</span>
                                <span v-if="isEndedVote">{{ __('Закончилось голосования') }}</span>
                                <br>
                                <small>Создаль: @{{ activeVote['creatorName'] }}</small>
                            </span>
                            <span v-if="!isEndedVote">
                                Время до окончения второй фазы<br>@{{ activeVote['timer'] }} сек.
                            </span>
                            <button v-if="activeVote['creatorId'] === user['id']" @click="cleanAndUpdateVote()"
                                class="btn btn-primary btn-sm">Обновить голосования</button><br>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <line-chart v-if="hasСhartData" :labels="getСhartData['title']"
                                    :data="getСhartData['data']">
                                </line-chart>
                            </div>
                            <div class="col-6">
                                <div v-if="hasWinnerVoter">
                                    <p>Победившее время и мероприятие</p>
                                    <ul>
                                        <li>
                                            @{{ getWinnerVoter['event_time']['event']['name'] }} -
                                            @{{ getWinnerVoter['event_time']['time']['time'] }}
                                        </li>
                                    </ul>
                                </div>
                                <div v-if="hasWinnerVoter">
                                    <p>Победившее</p>
                                    <ul>
                                        <li v-for="voice in getAllVoices"
                                            v-if="voice['event_time_id'] === getWinnerVoter['event_time_id']">
                                            @{{ voice['user']['name'] }} :
                                            @{{ voice['event_time']['event']['name'] }} -
                                            @{{ voice['event_time']['time']['time'] }}
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <p>Список участников</p>
                                    <ul>
                                        <li v-for="voice in getAllVoices">
                                            @{{ voice['user']['name'] }} :
                                            @{{ voice['event_time']['event']['name'] }} -
                                            @{{ voice['event_time']['time']['time'] }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div v-if="canVoteInSecondPhase" class="col-6">
                                <div v-if="!isEndedVote">
                                    <p>Согласиться с победившеем мероприятием и временем?</p>
                                    <button @click="voiceToWinner()" class="btn btn-primary">Согласиться</button>
                                    <button @click="refuseVoice()" class="btn btn-danger">Отказаться</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
