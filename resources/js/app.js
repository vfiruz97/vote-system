/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Suggestions from 'v-suggestions'
import 'v-suggestions/dist/v-suggestions.css' // you can import the stylesheets also (optional)
Vue.component('suggestions', Suggestions)

import { Doughnut, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

Vue.component('line-chart', {
    extends: Doughnut,
    mixins: [reactiveProp],
    props: ['labels', 'data', 'chartData'],
    mounted() {
        this.render();
    },
    methods: {
        render() {
            this.renderChart({
                labels: this.labels,
                datasets: [
                    {
                        label: 'The chart of vote',
                        backgroundColor: [
                            '#f87979',
                            '#44bcd8',
                            '#0a6298',
                            '#f6a35c',
                            '#cce7e8',
                            '#042f66',
                            '#edb879',
                            '#1979a9',
                            '#80391e',
                        ],
                        data: this.data,
                        responsive: true,
                        maintainAspectRatio: false,
                    }
                ]
            })
        }
    },
    watch: {
        data: function () {
            this.render();
        }
    }

});

var timer;
const app = new Vue({
    el: '#app',
    data() {
        return {
            query: '',
            selectedCountry: null,
            options: {
                inputClass: 'form-control'
            },
            minutes: [1, 10, 20, 30, 40, 50, 60],
            createVote: {
                duePhase1: 1,
                duePhase2: 1,
            },
            timeList: null,
            activeVote: null,
            user: null,
            createVoteFirstPhase: {
                time: '',
                event: '',
            },

        }
    },
    mounted() {
        Echo.private("getactivevote").listen("ChangedVoteData", (e) => {
            console.log(1);
            if (e.vote) {
                this.activeVote = e.vote;
            }
        });

        if (!this.hasActiveVote) {
            this.timeList = JSON.parse(this.$refs.basedata.getAttribute("timelist"));
            this.activeVote = JSON.parse(this.$refs.basedata.getAttribute("vote"))[0];
            this.user = JSON.parse(this.$refs.basedata.getAttribute("user"));
            this.createVoteFirstPhase.time = this.timeList[0]['id'];
        }

        if (this.hasActiveVote) {
            this.startTimer();
        }

    },
    computed: {
        canVoteInSecondPhase() {
            let can = false;
            for (let i = 0; i < this.getAllVoices.length; i++) {
                if (this.getAllVoices[i]['user_id'] === this.user['id']) {
                    if (this.getAllVoices[i]['event_time_id'] !== this.getWinnerVoter['event_time_id']) {
                        can = true;
                    } else {
                        can = false;
                    }
                }
            }
            return can;
        },
        hasActiveVote() {
            return this.activeVote ? true : false;
        },
        hasWinnerVoter() {
            return this.activeVote['winnerVoter'] !== null ? true : false;
        },
        getWinnerVoter() {
            return this.activeVote['winnerVoter']
        },
        hasСhartData() {
            return this.activeVote['chartData'] ? true : false;
        },
        getСhartData() {
            return this.activeVote['chartData']
        },
        getAllVoices() {
            return this.activeVote['allVoices']
        },
        canVoteInFirstPhase() {
            return this.hasActiveVote ? this.activeVote['userVoice'] === null : false;
        },
        isFirstPhaseVote() {
            return this.hasActiveVote ? this.activeVote['activePhase'] == 'first' : false;
        },
        isSecondPhaseVote() {
            return this.hasActiveVote ? this.activeVote['activePhase'] == 'second' : false;
        },
        isEndedVote() {
            return this.isFirstPhaseVote == this.isSecondPhaseVote;
        },
    },
    methods: {
        cleanAndUpdateVote() {
            let that = this;
            const url = `http://vote.loc/clean/vote`
            new Promise(resolve => {
                axios.post(url, { voteId: that.getWinnerVoter['vote_id'] })
                    .then(response => {
                        that.activeVote = response.data.vote;
                    }).catch(function (error) {
                        that.displayValidationError(error);
                    })
            });
        },
        voiceToWinner() {
            let that = this;
            const url = `http://vote.loc/voice/winner`
            new Promise(resolve => {
                axios.post(url, { eventTimeId: that.getWinnerVoter['event_time_id'], voteId: that.getWinnerVoter['vote_id'] })
                    .then(response => {
                        that.activeVote = response.data.vote;
                    }).catch(function (error) {
                        that.displayValidationError(error);
                    })
            });
        },
        refuseVoice() {
            let that = this;
            const url = `http://vote.loc/refuse/voice`
            new Promise(resolve => {
                axios.post(url, { voteId: that.getWinnerVoter['vote_id'] }).then(response => {
                    that.activeVote = response.data.vote;
                }).catch(function (error) {
                    that.displayValidationError(error);
                })
            })
        },
        createVoteFirstPhaseForm() {
            let that = this;
            that.createVoteFirstPhase['vote_id'] = that.activeVote['id'];
            const url = `http://vote.loc/store/event`
            new Promise(resolve => {
                axios.post(url, that.createVoteFirstPhase).then(response => {
                    that.activeVote = response.data.vote;
                }).catch(function (error) {
                    that.displayValidationError(error);
                })
            })
        },
        startTimer() {
            let that = this;
            timer = setInterval(function () {
                if (that.activeVote['timer'] > 0) {
                    that.activeVote['timer']--;
                } else {
                    if (that.activeVote['activePhase'] == 'first') {
                        that.getVote();
                    } else {
                        that.activeVote['activePhase'] = 'none';
                        clearInterval(timer);
                    }
                }
            }, 1000);
        },
        getVote() {
            let that = this;
            const url = `http://vote.loc/get/vote`
            new Promise(resolve => {
                axios.get(url, this.createVote).then(response => {
                    that.activeVote = response.data.vote;
                }).catch(function (error) {
                    that.displayValidationError(error);
                })
            })
        },
        createNewVote() {
            let that = this;
            const url = `http://vote.loc/home`
            new Promise(resolve => {
                axios.post(url, this.createVote).then(response => {
                    that.activeVote = response.data.vote;
                    that.startTimer();
                }).catch(function (error) {
                    that.displayValidationError(error);
                })
            })
        },
        displayValidationError(error) {
            if (error.response && error.response.status === 422) {
                if (error.response.data.errors) {
                    let errors = error.response.data.errors;
                    for (let field of Object.keys(errors)) {
                        alert(errors[field][0]);
                    }
                }
            } else {
                alert('Something went wrong');
            }
        },
        onEventInputChange(query) {
            if (query.trim().length === 0) {
                return null
            }
            const url = `http://vote.loc/get/event?event=${query}`
            return new Promise(resolve => {
                axios.get(url).then(response => {
                    const items = []
                    response.data.forEach((item) => {
                        items.push(item)
                    });
                    resolve(items)
                })
            })
        },
        onSearchItemSelected(item) {
            this.selectedSearchItem = item
        }
    }
});
