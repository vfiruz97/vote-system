<?php

namespace App\Http\Controllers;

use App\Repository\EventRepository;
use App\Repository\EventTimeRepository;
use App\Repository\TimeRepository;
use App\Repository\VoiceRepository;
use App\Repository\VoteRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    protected $vote;
    protected $time;
    protected $event;
    protected $eventTime;
    protected $voice;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        VoteRepository $vote,
        TimeRepository $time,
        EventRepository $event,
        EventTimeRepository $eventTime,
        VoiceRepository $voice
    ) {
        $this->middleware('auth');
        $this->vote = $vote;
        $this->time = $time;
        $this->event = $event;
        $this->eventTime = $eventTime;
        $this->voice = $voice;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $vote = $this->vote->getActiveVote();
        if ($vote != null)
            $this->vote->additionalField($vote);

        return view('home', [
            'timeList' => $this->time->getTimesByQuaters(),
            'vote' => $vote,
            'user' => $request->user(),
        ]);
    }

    public function storeVote(Request $request)
    {
        Validator::make($request->all(), [
            'duePhase1' => 'required|integer|in:1,10,20,30,40,50,60',
            'duePhase2' => 'required|integer|in:1,10,20,30,40,50,60',
        ])->validate();

        $get_vote_times = $this->vote->getVotePhrasesTimes($request);
        $vote = $this->vote->getActiveVote();
        if ($vote == null) $vote = $this->vote->create($get_vote_times);
        $this->vote->additionalField($vote);

        return response()->json(['vote' => $vote]);
    }

    public function storeEvent(Request $request)
    {
        Validator::make($request->all(), [
            'time' => 'required|integer|exists:times,id',
            'event' => 'required|string|min:2',
            'vote_id' => 'required|integer|exists:votes,id',
        ])->validate();

        if ($this->voice->getWhere([
            'vote_id' => $request->get('vote_id'),
            'user_id' => $request->user()->id
        ])->count() === 0) {
            $event = $this->event->saveIfNotExist($request->get('event'));
            $event_time = $this->eventTime->saveIfNotExist([
                'event_id' => $event->id,
                'time_id' => $request->get('time'),
            ]);
            $voice = $this->voice->create([
                'vote_id' => $request->get('vote_id'),
                'user_id' => $request->user()->id,
                'event_time_id' => $event_time->id,
            ]);
        }

        $vote = $this->vote->getActiveVote();
        if ($vote !== null)
            $this->vote->additionalField($vote);

        return response()->json(['vote' => $vote]);
    }

    public function getVote(Request $request)
    {
        $vote = $this->vote->getActiveVote();
        if ($vote != null)
            $this->vote->additionalField($vote);

        return response()->json(['vote' => $vote]);
    }

    public function getEvent(Request $request)
    {
        $data = [];
        $events = $this->event->getWhere([
            ['name', 'like', '%' . $request->get('event') . '%']
        ]);
        if ($events->count()) {
            $events->map(function ($event) use (&$data) {
                $data[] = $event->name;
                return 0;
            });
        }
        return response()->json($data);
    }

    public function voiceWinner(Request $request)
    {
        Validator::make($request->all(), [
            'eventTimeId' => 'required|integer|exists:voices,event_time_id',
            'voteId' => 'required|integer|exists:votes,id',
        ])->validate();

        $this->voice->update([
            'user_id' => $request->user()->id,
            'vote_id' => $request->get('voteId'),
        ], [
            'event_time_id' => $request->get('eventTimeId')
        ]);

        $vote = $this->vote->getActiveVote();
        if ($vote !== null)
            $this->vote->additionalField($vote);

        return response()->json(['vote' => $vote]);
    }

    public function refuseVoise(Request $request)
    {
        Validator::make($request->all(), [
            'voteId' => 'required|integer|exists:votes,id',
        ])->validate();

        $this->voice->delete([
            'user_id' => $request->user()->id,
            'vote_id' => $request->get('voteId'),
        ]);

        $vote = $this->vote->getActiveVote();
        if ($vote !== null)
            $this->vote->additionalField($vote);

        return response()->json(['vote' => $vote]);
    }

    public function cleanVote(Request $request)
    {
        Validator::make($request->all(), [
            'voteId' => 'required|integer|exists:votes,id',
        ])->validate();

        $this->vote->delete([
            'created_by' => $request->user()->id,
            'id' => $request->get('voteId'),
        ]);

        $vote = $this->vote->getActiveVote();
        if ($vote !== null)
            $this->vote->additionalField($vote);

        return response()->json(['vote' => $vote]);
    }
}
