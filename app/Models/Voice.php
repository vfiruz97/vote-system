<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vote_id',
        'user_id',
        'event_time_id',
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function event_time()
    {
        return $this->hasOne(EventTime::class, 'id', 'event_time_id')->with(['event', 'time']);
    }
}
