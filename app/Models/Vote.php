<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_time_first_phase',
        'end_time_first_phase',
        'start_time_second_phase',
        'end_time_second_phase',
        'created_by',
        'updated_by',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->created_by = request()->user()->id;
            $model->updated_by = request()->user()->id;
        });
        static::updating(function ($model) {
            $model->updated_by = request()->user()->id;
        });
    }

    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
