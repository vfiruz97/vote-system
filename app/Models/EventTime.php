<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventTime extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'time_id',
    ];

    public $table = "event_time";
    public $timestamps = false;

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function time()
    {
        return $this->hasOne(Time::class, 'id', 'time_id');
    }
}
