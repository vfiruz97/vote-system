<?php

namespace App\Repository;

use App\Models\Event;
use Illuminate\Http\Request;

class EventRepository
{
    protected $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function all()
    {
        return $this->event->all();
    }

    public function create(array $data)
    {
        return $this->event->create($data);
    }

    public function getWhere(array $con)
    {
        return $this->event->where($con)->get();
    }

    public function saveIfNotExist(string $name)
    {
        $event = $this->event->where('name', '=', $name)->first();
        if ($event == null)
            $event = $this->create(['name' => $name]);
        return $event;
    }
}
