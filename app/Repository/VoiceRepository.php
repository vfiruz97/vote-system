<?php

namespace App\Repository;

use App\Models\Voice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoiceRepository
{
    protected $voice;

    public function __construct(Voice $voice)
    {
        $this->voice = $voice;
    }

    public function all()
    {
        return $this->voice->all();
    }

    public function create(array $data)
    {
        return $this->voice->create($data);
    }

    public function getWhere(array $con)
    {
        return $this->voice
            ->with(['user', 'event_time'])
            ->where($con)
            ->get();
    }

    public function getVoices($vote_id)
    {
        return $this->voice
            ->select('id', DB::raw('id, vote_id, user_id, event_time_id, count(*) as total'))
            ->with(['user', 'event_time'])
            ->where('vote_id', '=', $vote_id)
            ->groupBy('event_time_id')
            ->get();
    }

    public function update(array $con, array $data)
    {
        return $this->voice->where($con)->update($data);
    }

    public function delete(array $con)
    {
        return $this->voice->where($con)->delete();
    }
}
