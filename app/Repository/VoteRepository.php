<?php

namespace App\Repository;

use App\Events\ChangedVoteData;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoteRepository
{
    protected $vote;
    protected $voice;

    public function __construct(Vote $vote, VoiceRepository $voice)
    {
        $this->vote = $vote;
        $this->voice = $voice;
    }

    public function all()
    {
        return $this->vote->all();
    }

    public function create(array $data)
    {
        return $this->vote->create($data);
    }

    public function delete(array $data)
    {
        return $this->vote->where($data)->delete();
    }

    public function getVotePhrasesTimes(Request $request): array
    {
        $start_time_first_phase = date("Y-m-d H:i:s");
        $end_time_first_phase = date(
            "Y-m-d H:i:s",
            strtotime("{$request->get('duePhase1', 10)} minutes")
        );
        $end_time_second_phase = date(
            "Y-m-d H:i:s",
            strtotime("{$request->get('duePhase2', 10)} minutes", strtotime($end_time_first_phase))
        );

        return [
            'start_time_first_phase' => $start_time_first_phase,
            'end_time_first_phase' => $end_time_first_phase,
            'start_time_second_phase' => $end_time_first_phase,
            'end_time_second_phase' => $end_time_second_phase,
        ];
    }

    public function getActiveVote()
    {
        $now = date('Y-m-d H:i:s');
        return $this->vote
            ->where([
                ['start_time_first_phase', '<', $now],
                ['end_time_second_phase', '>', $now],
            ])
            ->first();
    }

    public function additionalField(Vote &$vote)
    {
        $now = time();
        $diff = (strtotime($vote->end_time_first_phase) - $now);
        if ($diff > 0) {
            $vote->activePhase = 'first';
            $vote->timer = $diff;
        } else {
            $diff = (strtotime($vote->end_time_second_phase) - $now);
            if ($diff > 0) {
                $vote->activePhase = 'second';
                $vote->timer = $diff;
            } else {
                $vote->activePhase = 'none';
                $vote->timer = 0;
            }
        }

        $voice = $this->voice->getWhere([
            'vote_id' => $vote->id,
            'user_id' => request()->user()->id
        ]);
        if ($voice->count() === 0) {
            $vote->userVoice = null;
        } else {
            $vote->userVoice = $voice[0];
        }

        $vote->creatorName = $vote->creator->name;
        $vote->creatorId = $vote->creator->id;

        $this->getWinnerInogurate($vote);

        if ($vote->activePhase == 'second')
            event(new ChangedVoteData($vote));
    }

    private function getWinnerInogurate(Vote &$vote)
    {
        $winner = null;
        $chart_data = [];
        $voices = $this->voice->getVoices($vote->id);

        if ($voices->count()) {
            $max = 0;
            $voices->map(function ($voice) use (&$max, &$winner, &$chart_data) {
                if ($voice->total > $max) {
                    $max = $voice->total;
                    $winner = $voice;
                }
                $chart_data['title'][] = $voice->event_time->event->name . ' ' . $voice->event_time->time->time;
                $chart_data['data'][] = $voice->total;

                return $voice;
            });
        }
        $vote->winnerVoter = $winner;
        $vote->chartData = $chart_data;

        $voices = $this->voice->getWhere(['vote_id' => $vote->id]);
        $vote->allVoices = $voices;
    }
}
