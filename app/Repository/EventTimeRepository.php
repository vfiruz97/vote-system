<?php

namespace App\Repository;

use App\Models\EventTime;
use Illuminate\Http\Request;

class EventTimeRepository
{
    protected $eventTime;

    public function __construct(EventTime $eventTime)
    {
        $this->eventTime = $eventTime;
    }

    public function all()
    {
        return $this->eventTime->all();
    }

    public function create(array $data)
    {
        return $this->eventTime->create($data);
    }

    public function getWhere(array $con)
    {
        return $this->eventTime->where($con)->get();
    }

    public function saveIfNotExist(array $data)
    {
        $eventTime = $this->eventTime->where([
            ['event_id', '=', $data['event_id']],
            ['time_id', '=', $data['time_id']],
        ])->first();

        if ($eventTime == null)
            $eventTime = $this->create($data);

        return $eventTime;
    }
}
