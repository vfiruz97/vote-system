<?php

namespace App\Repository;

use App\Models\Time;
use Illuminate\Http\Request;

class TimeRepository
{
    protected $time;

    public function __construct(Time $time)
    {
        $this->time = $time;
    }

    public function all()
    {
        return $this->time->all();
    }

    public function create(array $data)
    {
        return $this->time->create($data);
    }

    public function getWhere(array $con)
    {
        return $this->time->where($con)->get();
    }

    public function getTimesByQuaters()
    {
        $data = [];
        $times = $this->time->orderBy('id', 'ASC')->get();
        
        foreach($times as $time){
            if(substr($time->time, 0, 2) >= date("H"))
                $data[] = $time;
        }
        return collect($data);
    }
}
