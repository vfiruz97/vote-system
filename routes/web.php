<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@storeVote')->name('home');
Route::get('/get/vote', 'HomeController@getVote')->name('get.vote');
Route::get('/get/event', 'HomeController@getEvent')->name('get.event');
Route::post('/store/event', 'HomeController@storeEvent')->name('get.event');
Route::post('/voice/winner', 'HomeController@voiceWinner')->name('voice.winner');
Route::post('/refuse/voice', 'HomeController@refuseVoise')->name('refuse.voice');
Route::post('/clean/vote', 'HomeController@cleanVote')->name('clean.vote');
