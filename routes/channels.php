<?php

use App\Models\Voice;
use App\Models\Vote;
use App\Repository\VoiceRepository;
use App\Repository\VoteRepository;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('users', function ($user) {
    return $user;
});

Broadcast::channel('getactivevote', function () {
    return true;
});
