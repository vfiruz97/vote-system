
## Vote system
# Задача
Необходимо разработать небольшую систему для планирования мероприятия. Планирование мероприятия
осуществляется голосованием в две фазы: в первой фазе голосования собираются предпочтения пользователей
по мероприятию и времени его проведения, во второй фазе голосования система принуждает пользователей к
единогласному решению.
1. При запуске страницы пользователю необходимо ввести имя и пароль, либо зарегистрироваться.
2. Пользователь инициирует старт голосования, указывая продолжительность первой и второй фазы
голосования в минутах. Следует обработать возможную конфликтную ситуацию, когда несколько
пользователей пытаются инициировать голосование. На любой момент времени активным быть может
только одно голосование.
3. Первая фаза голосования.
После старта голосования у пользователя появляется возможность заполнить форму со следующими
полями ввода:
a. Выпадающий список «Время», для выбора из вариантов: от текущего часа до 23 часов (с шагом
в 15 минут);
b. Текстовое поле «Мероприятие», для ввода произвольной строки;
4. В поле «Мероприятие» должен работать предиктивный ввод: системой предлагаются варианты
окончания из словаря, который пополняется на основе названий мероприятий, когда-либо
участвовавших в голосовании ранее (включая текущее голосование).
5. Отображается обратный отчёт времени завершения голосования и счётчик проголосовавших
пользователей, эта информация обновляется «на лету», без необходимости обновления страницы.
6. Когда время первой фазы голосования истекает, всем участникам голосования отображается следующая
информация:
a. Круговая диаграмма с двумя наборами данных: внутри круг должен быть разделён на секторы
по времени, а внешний круг будет отображать выбор мероприятий по каждому сектору. Таким
образом, общее количество внешних секторов должно соответствовать количеству уникальных
сочетаний "время-мероприятие".
b. Победившее время и мероприятие;
c. «Список участников мероприятия»: список пользователей, которые проголосовали за
победившее время и мероприятие;

7. Вторая фаза голосования начинается сразу, как только завершается первая фаза.
Пользователям, выбранное время или мероприятие которых не оказалось в числе победителей,
предлагается согласиться с мероприятием и временем, которые победили, либо отказаться. Если
пользователь соглашается, он включается в список участников мероприятия. В случае отказа участник
не включается в список. Вся информация об этом обновляется у всех пользователей «на лету».
8. Пользователь, начавший голосование, должен иметь возможность сбросить его итоги и инициировать
голосование заново.

## Seed
# Users
1. user_1@gmail.com / password
2. user_2@gmail.com / password
3. user_3@gmail.com / password
4. user_4@gmail.com / password
5. user_5@gmail.com / password
6. user_6@gmail.com / password
7. user_7@gmail.com / password
8. user_8@gmail.com / password
9. user_9@gmail.com / password
10. user_10@gmail.com / password

## Deploying

- rename .env.example to .env
- configure DB
- composer require
- php artisan migrate --seed
- npm install
- npm run dev
- php artisan websockets:serve
- php artisan config:cache
